import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register() {

	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the inputs
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ Password, setPassword ] = useState('');
	const [VPassword, setVPassword ] = useState('');

	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		if (firstName !== '' && lastName !== '' && Password !== '' && VPassword !== '' && Password === VPassword) { 

			setIsActive(true);
		} else{

			setIsActive(false);
		}
	}) 


	function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:2000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({

				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				Swal.fire({
					title: 'Duplicate email found!',
					icon: 'error',
					text: 'Please provide different email'
				})

			} else {

				// fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				fetch('http://localhost:2000/users/register', {

					method: "POST",
					headers: {

						'Content-Type': 'application/json'
					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: Password
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if(data === true) {

						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword('');
						setVPassword('');

						Swal.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Welcome to Bitzz!'
						});

						navigate('/home');
					} else {

						Swal.fire({
							title: 'Something went wrong',
							icon: 'error',
							text: 'Please try again'
						});
					};
				})
	
			}

		});
		
	}

	return (

		// (checkMail() === false)?
		(user.id !== null)? 
			<Navigate to="/login" />
		:		
			<Form onSubmit={(e) => {registerUser(e)}}>
			{/*<Form onSubmit={(e) => registerUser(e)}>*/}
				<Form.Group controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter First Name"
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="String"
						placeholder="Enter Last Name"
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email here"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					{/*<Form.Text className="text-muted">
						We will never share your email with anyone else.
					</Form.Text>*/}
				</Form.Group>
				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Password"
						value={Password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify Password"
						value={VPassword}
						onChange={e => setVPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?
					<Button variant="primary" type='submit' id=' submitBtn'> 
						Submit
					</Button>
					:
					<Button variant="danger" type='submit' id=' submitBtn' disabled> 
						Submit
					</Button>
				}

			</Form>
	)
}







































