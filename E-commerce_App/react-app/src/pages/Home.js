import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {

		title: "Bitzz Lifestyle",
		content: "Live Life Larger!",
		destination: '/products',
		label: "Grab Now!"
	};

	return (
		<>
			<Banner data={data} />
			<Highlights />
		</>
	)
};