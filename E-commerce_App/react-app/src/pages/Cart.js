import OrderCard from '../components/OrderCard';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, Link,  useNavigate } from 'react-router-dom';

export default function Cart(){

	const [ orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	const retrieveUserOrders = () => {
		
		fetch(`http://localhost:2000/orders/${user.id}/myOrders`, 
			{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setOrders(data.map(order => {
				return (
					<OrderCard key={order._id} order={order} />
				)
			}))
		})
	}

	useEffect(() => {
        if (user.id !== null) {
            retrieveUserOrders();
        }
        console.log(orders.length);
    }, [user.id]);

	// useEffect(() => {

	// 	fetch('http://localhost:2000/orders/myOrders', 
	// 		{ headers: { Authorization: `Bearer ${localStorage.getItem('token')}`}
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);

	// 		setOrders(data.map(order => {
	// 			return (
	// 				<OrderCard key={order._id} order={order} />
	// 			)
	// 		}))
	// 	})
	// }, [])


	return (
	 	<>
	 		{
	 			(user.id !== null) ?
	 				(	
	 					(orders.length !== 0) ?
	 						orders
	 						:
	 						<>
	 							<text><h1>Your cart is Empty!!</h1></text>	 				
	 							<Button className='btn btn-danger' as={Link} to='/products'>Buy Now</Button>
	 						</>
	 				)
	 			:
	 				<>
	 					<text><h1>You are not logged in!!</h1></text>	 				
	 					<Button className='btn btn-danger' as={Link} to='/login'>Log in to see your orders</Button>
	 				</>
	 		}
	 	</>
	);
};