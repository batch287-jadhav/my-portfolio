import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(props) {

	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const [ isActive, setIsActive ] = useState(false);

	useEffect(() => {

		if (email !== '' && password !== '') {

			setIsActive(true);
		} else {

			setIsActive(false);
		}
	}, [email, password])

	const retrieveUserDetails = (token, id) => {

		fetch(`http://localhost:2000/users/${id}/userDetails`, {

			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
		.catch((error) => {
      		console.error('Error fetching user details:', error);
    	});
	}

	function authentication(e) {

		e.preventDefault();

		fetch('http://localhost:2000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(typeof data.access !== 'undefined'){

				localStorage.removeItem('token');
				localStorage.setItem('token', data.access);
				localStorage.setItem('id', data.id);
				retrieveUserDetails(data.access, data.id);

				Swal.fire({
					title: 'Logged in Successfully',
					icon: 'success',
					text: 'Welcome to Bitzz!'
				})

			} else {

				Swal.fire({
					title: 'Authentication failed!',
					icon: 'error',
					text: 'Please fill in correct login details and try again!'
				})
			}

			setEmail('');
			setPassword('');
		})
	}

	return (

		(user.id !== null)? 
			<Navigate to="/home" />
		:

			<Form onSubmit={(e) => authentication(e)}>

			{/*<h1>Registration Form</h1>*/}
				<Form.Text className="text-dark fs-1 fw-bold">
						Login
				</Form.Text>

				<Form.Group controlId="email">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter Your Email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder=" Your Password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{ isActive ?
					<Button variant="success" type='submit' id=' submitBtn'> 
						Login
					</Button>
					:
					<Button variant="danger" type='submit' id=' submitBtn' disabled> 
						Login
					</Button>
				}

			</Form>
	)
}





























