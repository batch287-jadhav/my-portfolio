import ProductCard from '../components/ProductCard';
import { useState, useEffect, Fragment, useContext } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import UserContext from '../UserContext';

export default function Products(){

	const { user } = useContext(UserContext);

	const [ products, setProducts] = useState([]);


	const retrieveProducts = () => {
		fetch(`http://localhost:2000/products/all`)
		.then(res => res.json())
		.then(data => {

			setProducts(data)
		});

	}

	useEffect(() => {

		retrieveProducts();
		console.log(user.isAdmin);

	}, [])
	

	return (
	 	
		 	(user.isAdmin === true) ?
		 	<AdminView productsData={products} retrieveProducts={retrieveProducts}/>
		 	: 
		 	<UserView productsData={products}/>
	 	
	);
};