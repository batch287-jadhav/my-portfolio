import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Products from './pages/Products';
import Cart from './pages/Cart'

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { useState, useEffect, UseContext } from 'react';
import { UserProvider } from './UserContext';
import UserContext from './UserContext';
import ProductView from './components/ProductView'; 

export default function App() {
  
  const [ user, setUser ] = useState({

    id: null,
    isAdmin: null
  });

  const unsetUser = () => {

    localStorage.clear();
  };

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/${localStorage.getItem('id')}/details`, { headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== 'undefined'){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {

        setUser({
          id: null,
          isAdmin: null
        })
      }
    }) 
  }, [])

  return (

    <>
    <UserProvider value={{ user, setUser, unsetUser}} >
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
          <Route path='/home' element={<Home />} />
          <Route path='/' element={<Home />} />
          <Route path='/products' element={<Products />} />
          <Route path='/cart' element={<Cart />} />
          <Route path='/cart/:userId/myOrders' element={<Cart />} />
          <Route path='/products/:productId' element={<ProductView />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/register' element={<Register />} />
          <Route path='/*' element={<Error />} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
    </>

  );
 
}


