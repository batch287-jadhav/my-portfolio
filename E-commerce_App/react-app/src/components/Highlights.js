import { Row, Col, Card, CardImg } from 'react-bootstrap';
import earbuds from '../images/earbuds.jpg';
import earbuds1 from '../images/earbuds1.jpg';
import earbuds2 from '../images/earbuds2.jpg';
import headphone from '../images/headphone.jpg';
import speakers from '../images/speakers.jpg';
import smartwatch from '../images/smartWatch.jpg';
import mainpic from '../images/wearables.jpg';

export default function Highlights() {

	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 ">
					<Card.Img style={{ width: 320, height: 200 }} variant="top" src={earbuds2} />
					<Card.Body className=''>
						<Card.Title>
							<h2>Wireless Earbuds</h2>
						</Card.Title>
						<Card.Text>
							Elevate your sound experience with our wireless earbuds, offering seamless connectivity and immersive audio for your active lifestyle.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Img style={{height: 200 }} variant="top" src={smartwatch} />
					<Card.Body>
						<Card.Title>
							<h2>Smartwatches</h2>
						</Card.Title>
						<Card.Text>
							Stay ahead of the curve with our smartwatches, blending style and intelligence. Enhance your everyday with personalized connectivity and health insights
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Img style={{height: 200 }} variant="top" src={speakers} />
					<Card.Body>
						<Card.Title>
							<h2>Wireless Speakers</h2>
						</Card.Title>
						<Card.Text>
							Unleash auditory excellence with our wireless speakers. Whether you're hosting an event or craving tranquil moments, immerse in pristine soundscapes
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}