import{useState, useContext, useEffect} from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link,  useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const BuyNow = (productId) => {

		fetch('http://localhost:2000/orders/newOrder', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				products: [{productId, price, quantity: 1}]
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data) {

				Swal.fire({
					title: "Product ordered sucessfully!",
					icon: "success"
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		console.log(productId);

		fetch(`http://localhost:2000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	return (

		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}}>
					<Card>
						<Card.Body className='text-center'>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Battery Life:</Card.Subtitle>
							<Card.Text> 40 hrs</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text> PhP {price}</Card.Text>
							{
								(user.id !== null) ?
									<Button variant='primary' onClick={() => BuyNow(productId)}>Buy Now</Button>
									: 
									<Button className='btn btn-danger' as={Link} to='/login'>Log in to Buy</Button>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}




























































