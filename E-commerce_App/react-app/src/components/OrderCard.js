import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function OrderCard({order}) {
	
	console.log(order)

	const { products, totalAmount, _id } = order;


	return (
		<Row className="mt-3 mb-3">
			<Col xs={6}>
				<Card className='cardHighlight p-0'>
					<Card.Body>
						<Card.Title><h3>Your Order</h3></Card.Title>
						<Card.Subtitle>Items:</Card.Subtitle>
						<Card.Text className='ml-5, pl-5'>
  							{products.map((product, index) => (
  							<>
								<div key={index}>
		  							<p><b>Product:</b> {product.name}</p>
		  							<p><b>Price:</b> {product.price}</p>
		  							<p><b>Quantity:</b> {product.quantity}</p>
		  						
								</div>
								<Button className='bg-primary' as ={Link} to={`/products/${product.productId}`}>View</Button>
    						</>
  							))}					
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}