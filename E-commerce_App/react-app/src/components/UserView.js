import { Fragment, useState, useEffect } from 'react'
import ProductCard from "./ProductCard";

export default function UserView({productsData}) {

    // console.log(coursesData);

    const [products, setProducts] = useState([]);

    useEffect(() => {

        const productArr = productsData.map(products => {
     
            if(products.isActive === true){
                return (
                    <ProductCard product={products} key={products._id}/>
                )
            }else{
                return null;
            }
        });

        
        setProducts(productArr);

    }, [productsData]);

    return(
        <>
            {products}
        </>
    );
}
