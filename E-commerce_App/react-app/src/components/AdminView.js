import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	
	const { productsData, retrieveProducts } = props;


	const [productsId, setProductsId] = useState("");
	const [products, setProducts] = useState([]);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);


	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);


	const openEdit = (productsId) => {


		fetch(`http://localhost:2000/products/${productsId}`)
		.then(res => res.json())
		.then(data => {


			setProductsId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})


		setShowEdit(true);
	};


	const closeEdit = () => {

		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice(0);

	};

	const addProducts = (e) => {

		e.preventDefault()

		fetch(`http://localhost:2000/products/`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

			
				retrieveProducts();

				
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added."					
				})

				
				setName("")
				setDescription("")
				setPrice(0)

		
				closeAdd();

			} else {

				retrieveProducts();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const editProducts = (e, productsId) => {
		
		e.preventDefault();

		fetch(`http://localhost:2000/products/${ productsId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				retrieveProducts();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated."
				});

				closeEdit();

			} else {

				retrieveProducts();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

	useEffect(() => {

		const archive = (productsId, isActive) => {

			fetch(`http://localhost:2000/products/${ productsId }/archive`, {
				method: 'PATCH',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					retrieveProducts();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived/unarchived"
					});

				} else {

					retrieveProducts();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

			const activate = (productsId, isActive) => {

			fetch(`http://localhost:2000/products/${ productsId }/unarchive`, {
				method: 'PATCH',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					retrieveProducts();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived/unarchived"
					});

				} else {

					retrieveProducts();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const productsArr = productsData.map(product => {

			return(

				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
					
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="primary"
							size="sm"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
						{/* 
							- Display a red "Disable" button if course is "active"
							- Else display a green "Enable" button if course is "inactive"
						*/}
						{product.isActive
							?
							<Button 
								variant="danger" 
								size="sm" 
								onClick={() => archive(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="success"
								size="sm"
								onClick={() => activate(product._id, product.isActive)}
							>
								Enable
							</Button>
						}
					</td>
				</tr>

			)

		});


		setProducts(productsArr);

	}, [productsData, retrieveProducts]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={openAdd}>Add New Product</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

		
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProducts(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productsName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productsDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProducts(e, productsId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productsName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productsDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}
